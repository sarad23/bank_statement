import cv2
import numpy as np
import os
# from matplotlib import pyplot as plt

from lib.helpers.helpermethods import *


# filename = "hsbc_statement.png"
# filenames = ["hsbc_camera.jpg"]
# filename = "mandiri_statement.png"
# filename = "mandiri_camera.jpg"
# filename = "bca_statement.png"
# filename = "bca_camera.jpg"
def main(filename):
    results = []
    pic = cv2.imread(filename)
    # img = cv2.imread(filename, 0)
    ratio = pic.shape[0] / pic.shape[1]
    pic = cv2.resize(pic, (1280, int(1280 * ratio)), interpolation=cv2.INTER_CUBIC)
    img_x, index = findLogo(pic)
    bankName = index_to_name(index)
    results.append(bankName)
    return results


